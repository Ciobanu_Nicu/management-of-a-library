package ro.usv.ppaw.PPAW.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.usv.ppaw.PPAW.model.Student;
import ro.usv.ppaw.PPAW.repository.StudentRepository;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired StudentRepository studentRepository;

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(long id) {
        return studentRepository.findById(id);
    }

    @Override
    public void saveStudent(Student student){
        studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Student student){
        studentRepository.delete(student);
    }
}
