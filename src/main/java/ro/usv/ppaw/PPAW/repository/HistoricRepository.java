package ro.usv.ppaw.PPAW.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.usv.ppaw.PPAW.model.Historic;

import java.util.List;

@Repository
public interface HistoricRepository extends JpaRepository<Historic, Long> {

    List<Historic> findAll();
    Historic findById(long historic_id);
//    Historic findFirstByBook_id(long book_id);
//    Historic findFirstByStudent_id(long student_id);
}
