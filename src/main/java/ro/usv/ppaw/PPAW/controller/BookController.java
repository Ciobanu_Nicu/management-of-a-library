package ro.usv.ppaw.PPAW.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ro.usv.ppaw.PPAW.model.Book;
import ro.usv.ppaw.PPAW.model.Historic;
import ro.usv.ppaw.PPAW.service.BookService;
import ro.usv.ppaw.PPAW.service.HistoricService;

import javax.validation.Valid;
import java.util.List;

//TODO Nicu: Treat all cases before deleting, inserting, or refreshing tables with new objects.

@Controller
public class BookController {

    @Autowired
    BookService bookService;

    @Autowired
    HistoricService historicService;

    @GetMapping("/signup-book")
    public String showSignUpBookForm(Book book) {
        return "add-book";
    }

    //Getting all books from <book> table.
    @GetMapping(value = "/books")
    public String getBook(Model model)
    {
        model.addAttribute("book", bookService.findAll());
        return "indexb";
    }

    @GetMapping("/editbook/{book_id}")
    public String showUpdateBookForm(@PathVariable("book_id") long book_id, Model model) {

        Book bk = bookService.findById(book_id);
        model.addAttribute("book", bk);

        return "update-book";
    }
    //Deleting method using <book_id>
    @PostMapping("/updatebook/{book_id}")
    public String updateBook(@PathVariable("book_id") long book_id, @Valid Book bk, BindingResult result, Model model) {
        if (result.hasErrors()) {
            bk.setBook_id(book_id);
            return "update-book";
        }

        bookService.saveBook(bk);
        model.addAttribute("book", bookService.findAll());

        return "indexb";
    }

    //Adding method new book in table
    @PostMapping("/addbook")
    public String addBook(@Valid Book bk, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-book";
        }

        bookService.saveBook(bk);
        model.addAttribute("book", bookService.findAll());

        return "indexb";
    }

    @GetMapping("/deletebook/{book_id}")
    public String deleteBook(@PathVariable("book_id") long book_id, Model model) {

        Book bk = bookService.findById(book_id);

        bookService.deleteBook(bk);

        model.addAttribute("book", bookService.findAll());
        return "indexb";
    }
}
