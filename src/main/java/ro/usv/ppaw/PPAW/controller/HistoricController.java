package ro.usv.ppaw.PPAW.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ro.usv.ppaw.PPAW.model.Historic;
import ro.usv.ppaw.PPAW.service.BookService;
import ro.usv.ppaw.PPAW.service.HistoricService;
import ro.usv.ppaw.PPAW.service.StudentService;

import javax.validation.Valid;

@Controller
public class HistoricController {

    @Autowired
    HistoricService historicService;

    //add for display student name
    @Autowired
    StudentService studentService;

    @GetMapping("/signup-historic")
    public String showSignUpHistoricForm(Historic historic) {
        return "add-historic";
    }

    @GetMapping(value = "/historics")
    public String getHistorics(Model model)
    {
        model.addAttribute("student_name", studentService.findAll());
        model.addAttribute("historic", historicService.findAll());
        return "indexh";
    }

    @GetMapping("/edithistoric/{historic_id}")
    public String showUpdateHistoricForm(@PathVariable("historic_id") long historic_id, Model model) {
        Historic hist = historicService.findById(historic_id);
        model.addAttribute("historic", hist);
        return "update-historic";
    }

    @PostMapping("/updatehistoric/{historic_id}")
    public String updateHistoric(@PathVariable("historic_id") long historic_id, @Valid Historic hist, BindingResult result, Model model) {
        if (result.hasErrors()) {
            hist.setHistoric_id(historic_id);
            return "update-historic";
        }

        historicService.saveHistoric(hist);
        model.addAttribute("historic", historicService.findAll());
        return "indexh";
    }

    @PostMapping("/addhistoric")
    public String addHistoric(@Valid Historic hist, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-historic";
        }

        historicService.saveHistoric(hist);
        model.addAttribute("historic", historicService.findAll());
        return "indexh";
    }

    @GetMapping("/deletehistoric/{historic_id}")
    public String deleteHistoric(@PathVariable("historic_id") long historic_id, Model model) {
        Historic hist = historicService.findById(historic_id);
        historicService.deleteHistoric(hist);
        model.addAttribute("historic", historicService.findAll());
        return "indexh";
    }
}
