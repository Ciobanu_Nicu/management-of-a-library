package ro.usv.ppaw.PPAW.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "book", schema = "public")
public class Book {

    @Id
    @Column(name="book_id")
    long book_id;

    @Column(name="title")
    String title;

    @Column(name="author")
    String author;

    @Column(name="publishing")
    String publishing;

}
