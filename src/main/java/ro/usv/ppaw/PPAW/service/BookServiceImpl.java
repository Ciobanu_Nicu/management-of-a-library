package ro.usv.ppaw.PPAW.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.usv.ppaw.PPAW.model.Book;
import ro.usv.ppaw.PPAW.repository.BookRepository;

import java.util.List;

@Service
public class BookServiceImpl implements BookService{

    @Autowired
    BookRepository bookRepository;

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book findById(long book_id) {
        return bookRepository.findById(book_id);
    }

    @Override
    public void saveBook(Book book){
        bookRepository.save(book);
    }

    @Override
    public void deleteBook(Book book){
        bookRepository.delete(book);
    }

}
