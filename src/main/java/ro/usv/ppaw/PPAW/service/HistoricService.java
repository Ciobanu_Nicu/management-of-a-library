package ro.usv.ppaw.PPAW.service;

import ro.usv.ppaw.PPAW.model.Historic;

import java.util.List;

public interface HistoricService {

    public List<Historic> findAll();
    public Historic findById(long historic_id);
//    public Historic findFirstByBook_id(long book_id);
//    public Historic findFirstByStudent_id(long student_id);
    public void saveHistoric(Historic historic);
    public void deleteHistoric(Historic historic);
}
