package ro.usv.ppaw.PPAW.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.usv.ppaw.PPAW.model.Historic;
import ro.usv.ppaw.PPAW.repository.HistoricRepository;

import java.util.List;

@Service
public class HistoricServiceImpl implements HistoricService{

    @Autowired
    HistoricRepository historicRepository;

    @Override
    public List<Historic> findAll() {
        return historicRepository.findAll();
    }

    @Override
    public Historic findById(long historic_id) {
        return historicRepository.findById(historic_id);
    }

    @Override
    public void saveHistoric(Historic historic){
        historicRepository.save(historic);
    }

    @Override
    public void deleteHistoric(Historic historic){
        historicRepository.delete(historic);
    }

//    @Override
//    public Historic findFirstByBook_id(long book_id){
//        return historicRepository.findFirstByBook_id(book_id);
//    }
//
//    @Override
//    public Historic findFirstByStudent_id(long student_id){
//        return historicRepository.findFirstByStudent_id(student_id);
//    }


}
