package ro.usv.ppaw.PPAW.service;

import ro.usv.ppaw.PPAW.model.Book;

import java.util.List;

public interface BookService {
    public List<Book> findAll();
    public Book findById(long book_id);
    public void saveBook(Book book);
    public void deleteBook(Book book);
}
