package ro.usv.ppaw.PPAW.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ro.usv.ppaw.PPAW.model.Historic;
import ro.usv.ppaw.PPAW.model.Student;
import ro.usv.ppaw.PPAW.service.HistoricService;
import ro.usv.ppaw.PPAW.service.StudentService;

import javax.validation.Valid;
import java.util.List;

//TODO Nicu: Treat all cases before deleting, inserting, or refreshing tables with new objects.

@Controller
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    HistoricService historicService;

    @GetMapping("/signup")
    public String showSignUpForm(Student student) {
        return "add-student";
    }

    @GetMapping(value = "/students")
    public String getUsers(Model model)
    {
            model.addAttribute("student", studentService.findAll());
            return "index";
    }

    @GetMapping("/edit/{student_id}")
    public String showUpdateForm(@PathVariable("student_id") long student_id, Model model) {

        Student stud = studentService.findById(student_id);
        model.addAttribute("student", stud);
        return "update-student";

    }

    @PostMapping("/update/{student_id}")
    public String updateStudent(@PathVariable("student_id") long student_id, @Valid Student stud, BindingResult result, Model model) {
        if (result.hasErrors()) {
            stud.setStudent_id(student_id);
            return "update-student";
        }

        studentService.saveStudent(stud);
        model.addAttribute("student", studentService.findAll());

        return "index";
    }

    @PostMapping("/addstudent")
    public String addStudent(@Valid Student stud, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-student";
        }

        studentService.saveStudent(stud);
        model.addAttribute("student", studentService.findAll());

        return "index";
    }

    @GetMapping("/delete/{student_id}")
    public String deleteStudent(@PathVariable("student_id") long student_id, Model model) {
        Student student = studentService.findById(student_id);
        studentService.deleteStudent(student);
        model.addAttribute("student", studentService.findAll());

        return "index";
    }

}
