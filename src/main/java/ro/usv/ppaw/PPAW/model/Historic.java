package ro.usv.ppaw.PPAW.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "historic", schema = "public")
public class Historic {

    @Id
    @Column(name="historic_id")
    long historic_id;

    @Column(nullable=false)
    long student_id;

    @Column(nullable=false)
    long book_id;

    @ManyToOne(optional=false)
    @JoinColumn(name = "student_id", referencedColumnName="student_id", insertable=false, updatable=false)
    Student student;

    @ManyToOne(optional=false)
    @JoinColumn(name = "book_id", referencedColumnName="book_id", insertable=false, updatable=false)
    Book book;

    @Column(name="loan_days")
    int loan_days;

}
