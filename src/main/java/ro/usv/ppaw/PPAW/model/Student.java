package ro.usv.ppaw.PPAW.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "student", schema = "public")
public class Student {

    @Id
    @Column(name="student_id")
    long student_id;

    @Column(name="name")
    String name;

    @Column(name="faculty")
    String faculty;

    @Column(name="year")
    String year;

}
