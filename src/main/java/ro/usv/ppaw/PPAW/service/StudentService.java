package ro.usv.ppaw.PPAW.service;

import ro.usv.ppaw.PPAW.model.Student;

import java.util.List;

public interface StudentService {

    public List<Student> findAll();
    public Student findById(long id);
    public void saveStudent(Student student);
    public void deleteStudent(Student student);

}
